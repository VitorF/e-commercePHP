<?php 
namespace Hcode\Page;


use \Rain\Tpl;

class PageAdmin {

private $option  = [];
private $default = ["data"=>[]];
private $tpl;


public function __construct($opt = array()){
  $this->option = array_merge($this->default,$opt);
	$config   = ["tpl_dir"=>$_SERVER['DOCUMENT_ROOT']."/views/viewAdm/",
                 "cache_dir"=>$_SERVER['DOCUMENT_ROOT']."/views/view_cache/",
                 "debug"=>false];

    Tpl::configure($config);
    $this->tpl = new Tpl();
    $this->setData($this->option);
    $this->tpl->draw("header");
}



public function setTpl($conteudo,$data = array(),$return = false){
  $this->setData($data);
 return   $this->tpl->draw($conteudo,$return);
}

private function  setData($array =  array()){
 foreach ($array as $key => $value) {
$this->tpl->assign($key,$value);
 }
}


public function __destruct(){

$this->tpl->draw("footer");

}



}


 ?>